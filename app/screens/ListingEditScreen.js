import React from 'react';
import * as Yup from 'yup';
import { StyleSheet } from 'react-native';

import {
  AppForm,
  AppFormField,
  AppFormPicker,
  SubmitButton,
} from '../components/forms';
import Screen from '../components/Screen';
import CategoryPickerItem from '../components/CategoryPickerItem';
import FormImagePicker from '../components/forms/FormImagePicker';
// import useLocation from '../hooks/useLocation';

const validationSchema = Yup.object().shape({
  title: Yup.string().required().min(1).label('Title'),
  price: Yup.number().required().min(1).max(10000).label('Price'),
  description: Yup.string().label('Description'),
  category: Yup.object().required().nullable().label('Category'),
  images: Yup.array().min(1, 'Please select atleast one image'),
});

const categories = [
  {
    label: 'Furniture',
    value: 1,
    backgroundColor: 'tomato',
    icon: 'floor-lamp',
  },
  {
    label: 'Clothing',
    value: 2,
    backgroundColor: 'orange',
    icon: 'tshirt-v',
  },
  {
    label: 'Cameras',
    value: 3,
    backgroundColor: 'royalblue',
    icon: 'camera',
  },
  {
    label: 'Games',
    value: 4,
    backgroundColor: 'black',
    icon: 'gamepad-right',
  },
  {
    label: 'Cars',
    value: 5,
    backgroundColor: 'teal',
    icon: 'car',
  },
  {
    label: 'sports',
    value: 6,
    backgroundColor: 'forestgreen',
    icon: 'basketball',
  },
  {
    label: 'Movies & Music',
    value: 7,
    backgroundColor: 'darkslateblue',
    icon: 'video-input-component',
  },
  {
    label: 'Books',
    value: 8,
    backgroundColor: 'darkorchid',
    icon: 'book-open-page-variant',
  },
  {
    label: 'Other',
    value: 9,
    backgroundColor: 'dodgerblue',
    icon: 'alpha-r-box',
  },
];

const ListingEditScreen = () => {
  // const location = useLocation();

  return (
    <Screen style={styles.container}>
      <AppForm
        initialValues={{
          title: '',
          price: '',
          description: '',
          category: null,
          images: [],
        }}
        onSubmit={(values) => console.log(values)}
        validationSchema={validationSchema}
      >
        <FormImagePicker name="images" />
        <AppFormField maxLength={255} name="title" placeholder="Title" />
        <AppFormField
          keyboardType="numeric"
          maxLength={8}
          name="price"
          placeholder="Price"
        />
        <AppFormPicker
          items={categories}
          name="category"
          numberOfColumns={3}
          PickerItemComponent={CategoryPickerItem}
          placeholder="Category"
          width="50%"
        />
        <AppFormField
          maxLength={255}
          multiline
          name="description"
          numberOfLines={3}
          placeholder="Description"
        />
        <SubmitButton title="Post" />
      </AppForm>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
});

export default ListingEditScreen;

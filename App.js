import React from 'react';
// import AuthNavigator from './app/navigation/AuthNavigator';
import AppNavigator from './app/navigation/AppNavigator';
// import { Text, Button } from 'react-native';
// import { createStackNavigator } from '@react-navigation/stack';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import navigationTheme from './app/navigation/navigationTheme';
// import { MaterialCommunityIcons } from '@expo/vector-icons';

// import Screen from './app/components/Screen';
// import colors from './app/config/colors';

// import ListingsScreen from './app/screens/ListingsScreen';
// import ListingEditScreen from './app/screens/ListingEditScreen';
// import AccountScreen from './app/screens/AccountScreen';

// const Link = () => {
//   const navigation = useNavigation();
//   return (
//     <Button title="Click" onPress={() => navigation.navigate('TweetDetails')} />
//   );
// };

// const Tweets = ({ navigation }) => (
//   <Screen>
//     <Text>Tweets</Text>
//     <Button
//       title="View Tweet"
//       onPress={() => navigation.navigate('TweetDetails', { id: 1 })}
//     />
//   </Screen>
// );

// const TweetDetails = ({ route, navigation }) => {
//   const { id } = route.params;
//   return (
//     <Screen>
//       <Text>Tweet details {id}</Text>
//     </Screen>
//   );
// };

// const Stack = createStackNavigator();
// const StackNavigator = () => (
//   <Stack.Navigator>
//     <Stack.Screen name="Tweets" component={Tweets} />
//     <Stack.Screen name="TweetDetails" component={TweetDetails} />
//   </Stack.Navigator>
// );

// const AccountNavigator = () => (
//   <Screen>
//     <AccountScreen />
//   </Screen>
// );

// const AddNavigator = () => (
//   <Screen>
//     <ListingEditScreen />
//   </Screen>
// );

// const FeedNavigator = () => (
//   <Screen>
//     <ListingsScreen />
//   </Screen>
// );

// const Tab = createBottomTabNavigator();
// const TabNavigator = () => (
//   <Tab.Navigator
//     screenOptions={({ route }) => ({
//       tabBarIcon: ({ focused, color, size }) => {
//         let iconName;

//         if (route.name === 'Feed') {
//           iconName = focused ? 'home-circle' : 'home';
//           size = focused ? 40 : 20;
//         } else if (route.name === 'Add') {
//           iconName = focused ? 'plus-circle' : 'plus';
//           size = focused ? 40 : 20;
//         } else if (route.name === 'Account') {
//           iconName = focused ? 'account-card-details' : 'account';
//           size = focused ? 40 : 20;
//         }

//         return (
//           <MaterialCommunityIcons name={iconName} size={size} color={color} />
//         );
//       },
//     })}
//     tabBarOptions={{
//       activeTintColor: colors.primary,
//       inactiveBackgroundColor: colors.light,
//     }}
//   >
//     <Tab.Screen name="Feed" component={FeedNavigator} />
//     <Tab.Screen name="Add" component={AddNavigator} />
//     <Tab.Screen name="Account" component={AccountNavigator} />
//   </Tab.Navigator>
// );

export default function App() {
  return (
    <NavigationContainer theme={navigationTheme}>
      <AppNavigator />
    </NavigationContainer>
  );
}
